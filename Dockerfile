ARG tag
FROM nginx:$tag AS build

ARG COMMIT_SHA256=25f86f0bac1101b6512135eac5f93c49c63609e3

WORKDIR /build

# there are the same dependencies as for original nginx image
# https://github.com/nginxinc/docker-nginx/blob/70e44865208627c5ada57242b46920205603c096/stable/alpine/Dockerfile#L46
RUN apk add --no-cache --virtual .build-deps \
      alpine-sdk \
      bash \
      brotli-dev \
      curl \
      findutils \
      gcc \
      gd-dev \
      geoip-dev \
      libc-dev \
      libedit-dev \
      libxslt-dev \
      linux-headers \
      make \
      openssl-dev \
      pcre-dev \
      perl-dev \
      zlib-dev

RUN set -x \
    && curl -ssL "http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -o nginx.tar.gz \
    && git clone https://github.com/google/ngx_brotli.git \
    && cd /build/ngx_brotli \
    && git reset --hard $COMMIT_SHA256 \
    && cd - \
    && tar -zxC /build -f nginx.tar.gz \
    && cd nginx-$NGINX_VERSION \
    && CONFARGS=$(nginx -V 2>&1 | sed -n -e 's/^.*arguments: //p') \
    # to pass $CONFARGS variable correctly eval should be used. 
    # read more https://stackoverflow.com/a/21163341
    && eval ./configure --add-dynamic-module=/build/ngx_brotli $CONFARGS \
    && make \
    && make install

ARG tag
FROM nginx:$tag
LABEL MAINTAINER="Artyom Nosov <chip@unixstyle.ru>"

RUN apk --no-cache add brotli=~1.0.7

COPY --from=build /usr/lib/nginx/modules/ngx_http_brotli_static_module.so /usr/lib/nginx/modules
COPY --from=build /usr/lib/nginx/modules/ngx_http_brotli_filter_module.so /usr/lib/nginx/modules

COPY brotli.conf /etc/nginx/snippets/
COPY nginx.conf proxy_params ssl_params /etc/nginx/
